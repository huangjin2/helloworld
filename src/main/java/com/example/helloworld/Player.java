package com.example.helloworld;

import lombok.Data;

import java.util.List;

@Data
public class Player {
    private Long id;
    private String name;
    private int age;
    private List<String> avatars;
    private boolean test;
    private int testInt;
    private long testLong;
    private String testString;
}
