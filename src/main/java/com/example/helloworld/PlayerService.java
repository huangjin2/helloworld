package com.example.helloworld;

import java.util.HashMap;
import java.util.Map;

public class PlayerService {

    private Map<Long, Player> players = new HashMap<>();

    public void addPlayer(Player player) {
        players.put(player.getId(), player);
    }

    public Player getPlayer(long id) {
        return players.get(id);
    }

}
